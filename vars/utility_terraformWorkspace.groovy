def call(opts) {
    echo 'Terraform Workspace'
    def msgTitle = '[terraformWorkspace]'

    def buildTypeIs = utility_projectFlavor(currentBuild.projectName)
    buildTypeIs = buildTypeIs.toUpperCase()

    // terraformDir and Workspace opt var based on build type
    def terraformDir = opts.terraformDeployDir
    def terraformWorkspace = opts.terraformDeployWorkspace

    dir(terraformDir) {
        sh "terraform workspace select ${terraformWorkspace} || terraform workspace new ${terraformWorkspace}"
        env.TF_WORKSPACE = sh(returnStdout: true, script: 'terraform workspace show').trim()
    }
}
return this