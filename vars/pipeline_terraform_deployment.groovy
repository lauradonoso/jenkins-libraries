
def call(body) {
    def pipelineParams= [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = pipelineParams
    body()
    pipelineParams.each { println(it) }


    flag = [:]
    flag = utility_buildFeatureFlags(pipelineParams, 'TF_DEPLOYMENT')
    flag.each { println(it) }

    // Terraform vars
    pipelineParams.terraformHomeDir = pipelineParams.terraformHomeDir ?: '/tmp'
    pipelineParams.terraformBuildDir = pipelineParams.terraformBuildDir ?: 'deploy'
    pipelineParams.terraformDeployDir = pipelineParams.terraformDeployDir ?: 'deploy'
    pipelineParams.terraformDeployVarsDir = pipelineParams.terraformDeployVarsDir ?: 'tfvars'
    //pipelineParams.terraformDeployPlanFile = pipelineParams.terraformDeployPlanFile ?: 'pipeline.tfplan'

    pipelineParams.bindingVars = pipelineParams.bindingVars ?: [[id:"AWS_CREDENTIALS", type:"AWS", name:"AWS_CREDENTIALS", stages:["Prepare Workspace", "Terraform Init", "Terraform Workspace", "Terraform Plan", "Terraform Apply", "Terraform Destroy"]]]

    pipeline {
        agent any
        stages {
            stage ('Preparing environment'){
                steps { 
                    script { 
                        env.buildTypeIs = utility_projectFlavor(currentBuild.projectName)
                        echo "Build type is ${env.buildTypeIs}"
                        pipelineParams.deployRegion = params.deployRegion.trim()
                        pipelineParams.projectGitRef = params.deployConfigGitRef.trim()
                        pipelineParams.deployEnvironment = params.deployEnvironment.trim()
                        //pipelineParams.terraformDeployVarFile = pipelineParams.terraformDeployVarFile ?: "tfvars/${pipelineParams.deployEnvironment}"
                        pipelineParams.terraformDeployWorkspace = pipelineParams.deployEnvironment

                        // Update Build Display Name according to job and deployment type
                        def displayEnvironment = pipelineParams.deployEnvironment.capitalize()
                        currentBuild.displayName = "#${env.BUILD_NUMBER} [${displayEnvironment}] [${pipelineParams.deployRegion}] [${pipelineParams.projectGitRef}]"
                    }
                }
            }
            stage ('Download terraform') {
                steps { 
                    script { 
                        echo "Terraform version"
                        utility_terraformSetVersion(pipelineParams)
                        sh('aws --version')
                    }
                }
            }
            stage('Terraform Init') {
                when { expression { return flag['terraformInitFlag'] } }
                steps {
                    script {
                        utility_terraformInit(pipelineParams)
                    }
                }
            }
            stage('Terraform Workspace') {
                when { expression { return flag['terraformWorkspaceFlag'] } }
                steps {
                    script {
                        utility_terraformWorkspace(pipelineParams)
                    }
                }
            }
            stage ('Terraform Plan') {
                when { expression { return flag['terraformPlanFlag'] } }
                steps {
                    script {
                        utility_terraformPlan(pipelineParams)
                    }
                }
            }
            stage ('Terraform Apply') {
                when { expression { return flag['terraformApplyFlag'] } }
                steps {
                    script {
                        utility_terraformApply(pipelineParams)
                    }
                }
            }
            stage ('Terraform Destroy') {
                when { expression { return flag['terraformDestroyFlag'] } }
                steps {
                    script {
                        utility_terraformDestroy(pipelineParams)   
                    }
                }
            }
        }
    }
}

