def call(opts) {
    echo 'Terraform plan'
    def msgTitle = '[terraformPlan]'

    // Set local terraformVars based on build type
    def buildTypeIs = utility_projectFlavor(currentBuild.projectName)
    buildTypeIs = buildTypeIs.toUpperCase()

    // Configure command line vars and var file if they are set in the Jenkinsfile
    def commandLineOpts = '-input=false'
    def terraformVars = opts.terraformDeployVars
    def terraformVarFile = opts.terraformDeployVarFile
    def terraformDir = opts.terraformDeployDir
    def terraformPlanFile = opts.terraformDeployPlanFile
    def terraformDeployModules = opts.terraformDeployModules
    def terraformDetailedExitCode = opts.terraformDetailedExitCode ?: false
    def terraformVarFiles = opts.terraformDeployVarFiles instanceof Collection ? opts.terraformDeployVarFiles : []

    // Add Terraform Var Files if set
    if (terraformVarFile) {
        terraformVarFiles += terraformVarFile
    }
    if (terraformVarFiles) {
        echo "Parse Terraform Var Files for command"
        for (int i = 0; i < terraformVarFiles.size(); i++) {
            def file = terraformVarFiles[i]
            commandLineOpts += " -var-file=${file}"
        }
    }

    // Add tfplan output option
    commandLineOpts += " -out=${terraformPlanFile}"

    dir(terraformDir) {

        sh "terraform plan ${commandLineOpts}"
        
    }
}
return this
