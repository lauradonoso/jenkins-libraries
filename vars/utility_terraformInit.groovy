def call(opts) {
    echo 'Terraform Init'
    def msgTitle = '[terraformInit]'

    // Set local terraformVars based on build type
    def buildTypeIs = utility_projectFlavor(currentBuild.projectName)
    buildTypeIs = buildTypeIs.toUpperCase()

    // terraformDir opt var based on build type
    def commandLineOpts = '-input=false'
    def terraformBackendFile = null
    def terraformDir = opts.terraformBuildDir
    def terraformStateBucket = opts.terraformStateBucket
    def terraformStateLockTableName = opts.terraformStateLockTableName
    def terraformBackendRegion = opts.terraformBackendRegion
    def terraformBackendKey = opts.terraformBackendKey
    def terraformSetup = opts.terraformSetup


    // Fail if build is not a deployment and terraformBuildDir is not set in the pipeline
    if (!(opts.terraformBuildDir)) { error "${msgTitle} Missing required argument: terraformBuildDir" }

    if (opts.terraformBuildBackendFile) {
        terraformBackendFile = opts.terraformBuildBackendFile
    }

    // Add backend config file if it is specified
    if (terraformBackendFile) {
        commandLineOpts += " -backend-config=${terraformBackendFile}"
    }

    dir(terraformDir) {
        if (terraformStateBucket) {
            commandLineOpts = " -backend-config=bucket=${terraformStateBucket}"
        }

        if (terraformBackendRegion) {
            commandLineOpts += " -backend-config=region=${terraformBackendRegion}"
        }

        if (terraformBackendKey) {
            commandLineOpts += " -backend-config=key=${terraformBackendKey}"
        }

        if (terraformStateLockTableName) {
            commandLineOpts += " -backend-config=dynamodb_table=${terraformStateLockTableName}"
        }

        sh "terraform init ${commandLineOpts}"
    }
}
return this