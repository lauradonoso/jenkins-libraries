@NonCPS
def call(String projectName) {
    echo 'Evaluating project flavor'

    // if the build type is explicitly set in env, just use it
    if (env.BUILD_TYPE) {
        return env.BUILD_TYPE
    }

    // If projectName or env.BUILD_URL has / in it, replace urlencoded value
    projectName = projectName.replaceAll("%2F","/")
    def buildUrl = env.BUILD_URL.replaceAll("%252F","/")

    // If Project looks like MultiBranch Project, we don't want Branch Name, but the Project Name
    if (projectName == env.BRANCH_NAME) {
            projectName = buildUrl - ~/\/job\/${projectName}\/${env.BUILD_NUMBER}\/?$/
            projectName = projectName - ~/^.+\//
    }

    def flavor = (projectName =~ /_([^_]+)$/)
    flavor ? flavor[0][1] : projectName
}
return this