def call(opts) {
    echo 'Terraform apply'
    def msgTitle = '[terraformApply]'

    // Set local terraformVars based on build type
    def buildTypeIs = utility_projectFlavor(currentBuild.projectName)
    buildTypeIs = buildTypeIs.toUpperCase()

    // Configure command line vars and var file if they are set in the Jenkinsfile
    def commandLineOpts = '-input=false -auto-approve'
    def terraformDir = opts.terraformBuildDir
    def terraformPlanFile = opts.terraformBuildPlanFile
    def terraformStateBucket = opts.terraformStateBucket
    def terraformStateLockTableName = opts.terraformStateLockTableName
    def terraformBackendRegion = opts.terraformBackendRegion
    def terraformSetup = opts.terraformSetup
    def terraformDeployModules = opts.terraformDeployModules

    if ("${buildTypeIs}" == 'DEPLOY') {
        // Fail if required vars aren't set in the pipeline
        if (!(opts.terraformDeployDir)) { error "${msgTitle} Missing required argument: terraformDeployDir" }
        //if (!(opts.terraformDeployPlanFile)) { error "${msgTitle} Missing required argument: terraformDeployPlanFile" }

        //terraformPlanFile = opts.terraformDeployPlanFile
        terraformDir = opts.terraformDeployDir
    } else {
        // Fail if build is not a deployment and required terraform vars are not set
        if (!(opts.terraformBuildDir)) { error "${msgTitle} Missing required argument: terraformBuildDir" }
        //if (!(opts.terraformBuildPlanFile)) { error "${msgTitle} Missing required argument: terraformBuildPlanFile" }
    }

    dir(terraformDir) {

        sh "terraform apply ${commandLineOpts}"

    }
}
return this
