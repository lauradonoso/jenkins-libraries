@NonCPS
def override(flags, defaultTypeFlags, opts, optsBuildTypeFlags) {
    defaultTypeFlags.each {
        if (flags.containsKey(it)) {
            flags.put(it, true)
        }
    }
    if(opts) {
        opts.each {
            if (flags.containsKey(it.key)) {
                flags.put(it.key, it.value)
            }
        }
    }
    if(optsBuildTypeFlags) {
        optsBuildTypeFlags.each {
            if (flags.containsKey(it.key)) {
                flags.put(it.key, it.value)
            }
        }
    }
    return flags
}

def call(opts, projectType) {
    def buildType = utility_projectFlavor(currentBuild.projectName).toUpperCase()

    def overrideFlags = [:]
    def flagVars = [
        'terraformApplyFlag',
        'terraformDestroyFlag',
        'terraformInitFlag',
        'terraformPlanFlag',
        'terraformVaultInitFlag',
        'terraformWorkspaceFlag',
    ]
    flagVars.each{ String it -> overrideFlags[it] = false }

    // Set default list of stages for each Build Type
    def defaultFlagYaml = libraryResource "featureFlags_${projectType.toLowerCase()}.yaml"
    def defaultPipelineFlags = readYaml text: defaultFlagYaml

    // Prepare final list of flags for pipeline, including overrides of defaults and opt-in for non-default
    println("Build ${buildType.toUpperCase()} build type")

    def defaultBuildTypeFlags
    def optBuildTypeFlags

    switch (buildType.toUpperCase()) {
        case 'DEPLOY':
            defaultBuildTypeFlags = defaultPipelineFlags.deployFlags
            optBuildTypeFlags = (opts && opts.overrideBuildStages && opts.overrideBuildStages['deploy']) ? opts.overrideBuildStages['deploy'] : [:]
            break
        case 'PLAN':
            defaultBuildTypeFlags = defaultPipelineFlags.planFlags
            optBuildTypeFlags = (opts && opts.overrideBuildStages && opts.overrideBuildStages['plan']) ? opts.overrideBuildStages['plan'] : [:]
            break

        case 'REMOVE':
            defaultBuildTypeFlags = defaultPipelineFlags.removeFlags
            optBuildTypeFlags = (opts && opts.overrideBuildStages && opts.overrideBuildStages['remove']) ? opts.overrideBuildStages['remove'] : [:]
            break
        default:
            error "Build type is not correct. The acceptable values are: \n\t" +
                    "DEPLOY, PLAN, REMOVE"
            break
    }

    // Generate final list of flags with all overrides
    finalFlags = override(overrideFlags, defaultBuildTypeFlags, opts, optBuildTypeFlags)

    return finalFlags
}
