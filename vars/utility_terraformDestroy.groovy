def call(opts) {
    echo 'Terraform destroy'
    def msgTitle = '[terraformDestroy]'

    // Check required var exists
    if (!(opts.terraformDeployDir)) { error "${msgTitle} Missing required argument: terraformDeployDir" }

    // Configure command line vars and var file if they are set in the Jenkinsfile
    def commandLineOpts = '-input=false -auto-approve'
    def terraformVars = opts.terraformDeployVars
    def terraformVarFile = opts.terraformDeployVarFile
    def terraformDir = opts.terraformDeployDir
    def terraformSetup = opts.terraformSetup
    def terraformWorkspace = opts.terraformDeployWorkspace
    def terraformDeployModules = opts.terraformDeployModules
    def terraformVarFiles = opts.terraformDeployVarFiles instanceof Collection ? opts.terraformDeployVarFiles : []

    // Add Terraform Var Files if set
    if ( !(terraformVarFile == null) ) {
        terraformVarFiles += terraformVarFile
    }
    if (terraformVarFiles) {
        echo "Parse Terraform Var Files for command"
        for (int i = 0; i < terraformVarFiles.size(); i++) {
            def file = terraformVarFiles[i]
            commandLineOpts += " -var-file=${file}"
        }
    }

    dir(terraformDir) {
        def terraformIgnoreError = ''

        // If this is Terraform setup remove, ignore terraform apply error since final state cannot be updated because the backend
        // resources are removed.
        if ( !(terraformSetup == null) ) {
            terraformIgnoreError = ' || true'
        }
        sh "terraform destroy ${commandLineOpts}${terraformIgnoreError}"
        
        // Delete workspace after destroying all resources
        if ( !(terraformWorkspace == null) ) {
            // Set TF_WORSPACE environment var to 'default' first
            env.TF_WORKSPACE = 'default'
            sh "terraform workspace delete ${terraformWorkspace}"
        }
    }
}
return this
