def call(opts) {
    echo 'Set Terraform Version'
    def msgTitle = '[terraformSetVersion]'
  
    def terraformVersion = opts.terraformVersion

    if (!(terraformVersion)) { error "${msgTitle} Missing required argument: terraformVersion" }

    def terraformHomeDir = opts.terraformHomeDir ?: '/tmp'

    def terraformSourceFile = "terraform_${terraformVersion}_linux_amd64.zip"
    def terraformDownloadUrl = opts.terraformDownloadUrl ?: "https://releases.hashicorp.com/terraform/${terraformVersion}/${terraformSourceFile}"

    def currentVersion = sh(returnStdout: true, script: 'terraform version | head -1 | awk \'{print $2}\'')

    echo "Current version: ${currentVersion}"
    echo "Setting version to v${terraformVersion}..."
  
    // S3 doesn't contain TF version - Pull from external source
    echo "Downloading Terraform version ${terraformVersion} from external source."
    sh "curl -fL ${terraformDownloadUrl} -o ./${terraformSourceFile}"
    uploadRequired = true
    

    // Unzip Terraform file to home directory
    sh "unzip -o ${terraformSourceFile} -d ${terraformHomeDir}"

    // Set path precedence for the new terraform binary (since we can't overwrite existing bin as non-root user)
    env.PATH = terraformHomeDir + ':' + env.PATH

    sh("terraform version")
}